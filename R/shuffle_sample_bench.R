microbenchmark::microbenchmark(
  shuffle_sample_getn(1e5, 4, 1e7),
  times=100
)

# Unit: microseconds
# expr min  lq  mean median  uq  max neval
# shuffle_sample_getn(1e+05, 4, 1e+07) 2.4 2.5 2.721    2.6 2.7 13.3   100

n_shuffle_sample<-shuffle_sample_getn(1e5, 4, 1e7)
microbenchmark::microbenchmark(
  shuffle_sample_do(1e5, 4, n_shuffle_sample$n_shuffle, n_shuffle_sample$n_sample),
  times=3
)