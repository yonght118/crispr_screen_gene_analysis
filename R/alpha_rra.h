#include <RcppArmadillo.h>
#include <omp.h>
// [[Rcpp::depends(RcppArmadillo)]]

inline std::tuple<double, arma::uword> alpha_rra_base(const arma::vec& norm_ranks, double alpha, 
  bool min1rank) {
  // min1rank: use at least 1 rank for scoring; same as MAGeCK implementation
  arma::vec sorted_ranks = arma::sort(norm_ranks);
  arma::uword n = sorted_ranks.n_elem;
  arma::uword n_good = 0;
  
  double rra_score = 1.0; // max pbeta can return is 1.0
  
  
  for (arma::uword i=0;i<n; i++) {
    bool below_alpha = sorted_ranks(i)<=alpha;
    if (below_alpha) n_good+=1;
    if (below_alpha || (i==0 && min1rank)) {
      double this_pbeta = R::pbeta (sorted_ranks(i), i+1, n-i, true, false);  
      if (this_pbeta<rra_score) rra_score = this_pbeta;
    }
  }
  
  return std::make_tuple(rra_score, n_good);
}

inline Rcpp::List alpha_rra_list(const arma::vec& norm_ranks, double alpha, 
  bool min1rank) {
  const auto result = alpha_rra_base(norm_ranks, alpha, min1rank);
  
  return Rcpp::List::create(
    Rcpp::Named("rra_score") = std::get<0>(result),
    Rcpp::Named("n_good") = std::get<1>(result)
  );
}

inline arma::rowvec alpha_rra_vec(const arma::vec& norm_ranks, double alpha, 
  bool min1rank) {
  const auto result = alpha_rra_base(norm_ranks, alpha, min1rank);
  
  return arma::rowvec({std::get<0>(result),static_cast<double>(std::get<1>(result))});
}


inline double alpha_rra_base_scoreonly(const arma::vec& norm_ranks, double alpha, 
  bool min1rank) {
  // min1rank: use at least 1 rank for scoring; same as MAGeCK implementation
  arma::vec sorted_ranks = arma::sort(norm_ranks);
  arma::uword n = sorted_ranks.n_elem;
  arma::uword n_good = 0;
  
  double rra_score = 1.0; // max pbeta can return is 1.0
  
  
  for (arma::uword i=0;i<n; i++) {
    bool below_alpha = sorted_ranks(i)<=alpha;
    if (below_alpha) n_good+=1;
    if (below_alpha || (i==0 && min1rank)) {
      double this_pbeta = R::pbeta (sorted_ranks(i), i+1, n-i, true, false);  
      if (this_pbeta<rra_score) rra_score = this_pbeta;
    }
  }
  
  return rra_score;
}

// [[Rcpp::export]]
inline double Cpp_alpha_rra_mageck_scoreonly(const arma::vec& norm_ranks, double alpha) {
  return alpha_rra_base_scoreonly(norm_ranks, alpha, true);
}

// [[Rcpp::export]]
inline arma::rowvec Cpp_alpha_rra_mageck_vec(const arma::vec& norm_ranks, double alpha) {
  return alpha_rra_vec(norm_ranks, alpha, true);
}


// [[Rcpp::export]]
inline Rcpp::List Cpp_alpha_rra_mageck(const arma::vec& norm_ranks, double alpha) {
  return alpha_rra_list(norm_ranks, alpha, true);
}

// [[Rcpp::export]]
inline double Cpp_alpha_rra_scoreonly(const arma::vec& norm_ranks, double alpha) {
  return alpha_rra_base_scoreonly(norm_ranks, alpha, false);
}

// [[Rcpp::export]]
inline arma::rowvec Cpp_alpha_rra_vec(const arma::vec& norm_ranks, double alpha) {
  return alpha_rra_vec(norm_ranks, alpha, false);
}

// [[Rcpp::export]]
inline Rcpp::List Cpp_alpha_rra(const arma::vec& norm_ranks, double alpha) {
  return alpha_rra_list(norm_ranks, alpha, false);
}
