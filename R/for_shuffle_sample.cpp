// Parallel version using OpenMP

#include <RcppArmadillo.h>
#include <omp.h>
#include "alpha_rra.h"
// [[Rcpp::depends(RcppArmadillo)]]
// [[Rcpp::plugins(openmp)]]

template<typename M, double F(const M&)>
Rcpp::NumericVector Cpp_shuffle_sample_for_template_cpp(
    const M& data,
    const arma::umat& ushuffles,
    const arma::umat& usamples,
    const int n_threads = 1
  ) {
  unsigned int n_shuffles = ushuffles.n_cols;
  unsigned int n_samples = usamples.n_cols;
  arma::Col<double> result = arma::zeros(n_shuffles*n_samples);

  #pragma omp parallel for num_threads(n_threads)
  for (arma::uword i=0;i<n_shuffles; i++) {
    arma::uvec ushuffle_i = ushuffles.col(i);
    M shuffle_i = data.cols(ushuffle_i);
    arma::uword result_index_i = i*n_samples;
    
    for (arma::uword j=0;j<n_samples; j++) {
      arma::uvec usample_j = usamples.col(j);
      result[result_index_i+j] = F(shuffle_i.cols(usample_j));
    }
  }
  return Rcpp::NumericVector(result.begin(), result.end());
}

inline double f_accu(const arma::Row<double>& row) {
  return arma::accu(row);
}

inline double f_mean(const arma::Row<double>& row) {
  return arma::mean(row);
}

inline double f_median(const arma::Row<double>& row) {
  return arma::median(row);
}

inline double f_wmean(const arma::Mat<double>& mat) {
  return arma::accu( mat.row(0) % mat.row(1) ) / arma::accu(mat.row(1));
}


// [[Rcpp::export]]
Rcpp::NumericVector Cpp_shuffle_sample_for_sum_cpp(
    const arma::Row<double>& data,
    const arma::umat& ushuffles,
    const arma::umat& usamples,
    const int n_threads = 1
  ) {
  return Cpp_shuffle_sample_for_template_cpp<arma::Row<double>, f_accu>(
    data, ushuffles, usamples, n_threads
  );
}

// [[Rcpp::export]]
Rcpp::NumericVector Cpp_shuffle_sample_for_mean_cpp(
    const arma::Row<double>& data,
    const arma::umat& ushuffles,
    const arma::umat& usamples,
    const int n_threads = 1
  ) {
  return Cpp_shuffle_sample_for_template_cpp<arma::Row<double>, f_mean>(
    data, ushuffles, usamples, n_threads
  );
}

// [[Rcpp::export]]
Rcpp::NumericVector Cpp_shuffle_sample_for_median_cpp(
    const arma::Row<double>& data,
    const arma::umat& ushuffles,
    const arma::umat& usamples,
    const int n_threads = 1
  ) {
  return Cpp_shuffle_sample_for_template_cpp<arma::Row<double>, f_median>(
    data, ushuffles, usamples, n_threads
  );
}

// [[Rcpp::export]]
Rcpp::NumericVector Cpp_shuffle_sample_for_wmean_cpp(
    const arma::Mat<double>& data,
    const arma::umat& ushuffles,
    const arma::umat& usamples,
    const int n_threads = 1
  ) {
  return Cpp_shuffle_sample_for_template_cpp<arma::Mat<double>, f_wmean>(
    data, ushuffles, usamples, n_threads
  );
}


// [[Rcpp::export]]
Rcpp::NumericVector Cpp_shuffle_sample_for_Rfunc_cpp(
    const arma::Col<double>& data,
    const arma::umat& ushuffles,
    const arma::umat& usamples,
    const Rcpp::Function f
  ) {
  // no threads when calling R API
  unsigned int n_shuffles = ushuffles.n_cols;
  unsigned int n_samples = usamples.n_cols;
  arma::vec result = arma::zeros(n_shuffles*n_samples);

  for (arma::uword i=0;i<n_shuffles; i++) {
    arma::uvec ushuffle_i = ushuffles.col(i);
    arma::Col<double> shuffle_i = data(ushuffle_i);
    arma::uword result_index_i = i*n_samples;
    
    for (arma::uword j=0;j<n_samples; j++) {
      arma::uvec usample_j = usamples.col(j);
      arma::vec x = shuffle_i(usample_j);
      Rcpp::NumericVector fresult = f(x);
      result[result_index_i+j] = fresult[0];
    }
  }
  return Rcpp::NumericVector(result.begin(), result.end());
}


template <typename V, typename P>
struct VFunc
{
    typedef V (*type)(const arma::Col<V>&, P);
};

template <typename V, typename P>
inline Rcpp::NumericVector Cpp_shuffle_sample_for_cppfunc_cpp(
    const arma::Col<V>& data,
    const arma::umat& ushuffles,
    const arma::umat& usamples,
    typename VFunc<V,P>::type f_,
    const P param,
    const int n_threads = 1
) {
  unsigned int n_shuffles = ushuffles.n_cols;
  unsigned int n_samples = usamples.n_cols;
  arma::Col<V> result = arma::zeros(n_shuffles*n_samples);

  #pragma omp parallel for num_threads(n_threads)
  for (arma::uword i=0;i<n_shuffles; i++) {
    arma::uvec ushuffle_i = ushuffles.col(i);
    arma::Col<V> shuffle_i = data(ushuffle_i);
    arma::uword result_index_i = i*n_samples;
    
    for (arma::uword j=0;j<n_samples; j++) {
      arma::uvec usample_j = usamples.col(j);
      result[result_index_i+j] = f_(shuffle_i(usample_j), param);
    }
  }
  return Rcpp::NumericVector(result.begin(), result.end());
  
}

template <typename M, typename P>
struct MFunc
{
    typedef arma::Row<M> (*type)(const arma::Col<M>&, P);
};

template <typename M, typename P, int n_func>
inline arma::Mat<M> Cpp_shuffle_sample_for_cppfuncmat_cpp(
    const arma::Col<M>& data,
    const arma::umat& ushuffles,
    const arma::umat& usamples,
    typename MFunc<M,P>::type f_,
    const P param,
    const int n_threads = 1
) {
  unsigned int n_shuffles = ushuffles.n_cols;
  unsigned int n_samples = usamples.n_cols;
  arma::Mat<M> result = arma::zeros(n_shuffles*n_samples, n_func);

  #pragma omp parallel for num_threads(n_threads)
  for (arma::uword i=0;i<n_shuffles; i++) {
    arma::uvec ushuffle_i = ushuffles.col(i);
    arma::Col<M> shuffle_i = data(ushuffle_i);
    arma::uword result_index_i = i*n_samples;
    
    for (arma::uword j=0;j<n_samples; j++) {
      arma::uvec usample_j = usamples.col(j);
      result.row(result_index_i+j) = f_(shuffle_i(usample_j), param);
    }
  }
  return result;
  
}
  
template <typename V, typename P>
inline Rcpp::NumericVector Cpp_shuffle_sample_for_cppfunc_cpp(
    const arma::Col<V>& data,
    const arma::umat& ushuffles,
    const arma::umat& usamples,
    const SEXP f,
    const P param,
    const int n_threads = 1
  ) {

  typename VFunc<V,P>::type f_ = *Rcpp::XPtr<typename VFunc<V,P>::type>(f);
  return Cpp_shuffle_sample_for_cppfunc_cpp(
    data, ushuffles, usamples, f_, param, n_threads
  );
}

// [[Rcpp::export]]
Rcpp::NumericVector Cpp_shuffle_sample_for_cppfuncdbl_cpp(
    const arma::Col<double>& data,
    const arma::umat& ushuffles,
    const arma::umat& usamples,
    const SEXP f,
    const int n_threads = 1) {
  return Cpp_shuffle_sample_for_cppfunc_cpp<double, double>(
    data, ushuffles, usamples, f, 0.0, n_threads
  );
}


// [[Rcpp::export]]
Rcpp::NumericVector Cpp_shuffle_sample_for_alpharra_cpp(
    const arma::Col<double>& data,
    const arma::umat& ushuffles,
    const arma::umat& usamples,
    double alpha,
    const int n_threads = 1) {
  return Cpp_shuffle_sample_for_cppfunc_cpp<double, double>(
    data, ushuffles, usamples, &Cpp_alpha_rra_mageck_scoreonly, alpha, n_threads
  ); 
}

// [[Rcpp::export]]
arma::Mat<double> Cpp_shuffle_sample_for_alpharra_mat_cpp(
    const arma::Col<double>& data,
    const arma::umat& ushuffles,
    const arma::umat& usamples,
    double alpha,
    const int n_threads = 1) {
  return Cpp_shuffle_sample_for_cppfuncmat_cpp<double, double, 2>(
    data, ushuffles, usamples, &Cpp_alpha_rra_mageck_vec, alpha, n_threads
  ); 
}

