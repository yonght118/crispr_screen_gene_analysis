
get_execute_score_func <- function(func, param, threads=1, for_permut = FALSE) {
  ret<-NULL
  if (is.character(func)) {
    if (func[1]=="alpharra") {
      if (for_permut) {
        ret<-function(statistics, shuffles, samples) {
          Cpp_shuffle_sample_for_alpharra_cpp(statistics, shuffles-1, samples-1, 
            alpha=param, n_threads=threads)
        }
        attr(ret, "colnames") <- c("rrascore")
      }
      else {
        ret<-function(statistics, shuffles, samples) {
          Cpp_shuffle_sample_for_alpharra_mat_cpp(statistics, shuffles-1, samples-1, 
            alpha=param, n_threads=threads)
        }
        attr(ret, "colnames") <- c("rrascore","n_good")
      }
      attr(ret, "direction") <- "desc"
    } else if (func[1]=="sum") {
      ret<-function(statistics, shuffles, samples) {
        Cpp_shuffle_sample_for_sum_cpp(statistics, shuffles-1, samples-1, 
          n_threads=threads)
      }
      attr(ret, "colnames") <- "sum"
    } else if (func[1]=="mean") {
      ret<-function(statistics, shuffles, samples) {
        Cpp_shuffle_sample_for_mean_cpp(statistics, shuffles-1, samples-1, 
          n_threads=threads)
      }
      attr(ret, "colnames") <- "mean"
    } else if (func[1]=="median") {
      ret<-function(statistics, shuffles, samples) {
        Cpp_shuffle_sample_for_median_cpp(statistics, shuffles-1, samples-1, 
          n_threads=threads)
      }
      attr(ret, "colnames") <- "median"
    } else if (func[1]=="weighted.mean") {
      ret<-function(statistics, shuffles, samples) {
        Cpp_shuffle_sample_for_wmean_cpp(statistics, shuffles-1, samples-1, 
          n_threads=threads)
      }
      attr(ret, "colnames") <- "weighted.mean"
    }
  }
  if (is.null(ret))
    stop(str_glue("unknown score function {format(func)}"))
  ret
  
}

permutation_p_frommat <- function(
  indexes_mat, statistics, n_permut=9999, 
  func = NULL, param = NULL, decreasing = NULL,
  seed=NULL, jump=NULL, exact_n_permut=FALSE,
  threads = 1) {
  if (is.matrix(statistics)) {
    n_elem<-ncol(statistics)
  } else {
    n_elem<-length(statistics)
  }
  
  n_pick<-nrow(indexes_mat)
  n_shufflesample<-shuffle_sample_getn(n_elem, n_pick, N=n_permut)
  shufflesamples<-shuffle_sample_do(n_elem, n_pick, 
    n_shufflesample$n_shuffle, n_shufflesample$n_sample,
    seed=seed, jump=jump)
  
  identity_map<-matrix(1:n_elem, ncol=1) # 1:1 mapping for scoring from indexes_mat
  execute_score_func<-get_execute_score_func(func, param, threads)
  execute_scorepermut_func<-get_execute_score_func(func, param, threads, for_permut=TRUE)

  scores<-execute_score_func(statistics, identity_map, indexes_mat)
  ret_colnames<-attr(execute_score_func, "colnames")
  
  if (is.matrix(scores)) {
    scores_mat<-scores
    if (is.null(ret_colnames)) {
      ret_colnames<-str_c("score_", 1:ncol(scores))
    }
    colnames(scores_mat)<-ret_colnames
    scores<-scores_mat[,1]
    scores_df<-as_tibble(scores_mat)
  } else {
    if (is.null(ret_colnames)) {
      ret_colnames<-"score"
    }
    scores_df<-as_tibble_col(scores, column_name=ret_colnames)
  }
  scores_df<- bind_cols(
    tibble(
      id = colnames(indexes_mat),
      items = n_pick
    ),
    scores_df
  )

  permuts<-execute_scorepermut_func(statistics, 
    shufflesamples$shuffle_mat, shufflesamples$sample_mat)
  
  if (is.null(decreasing)) {
    if (identical(attr(execute_score_func, "direction"), "desc"))
      decreasing<-TRUE
    else
      decreasing<-FALSE
  }

  sorted<-sort(permuts, decreasing = decreasing)
  if (exact_n_permut) sorted<-sorted[1:n_permut]
  if (decreasing)
    b<-Cpp_count_lte(sorted, scores, threads)
  else
    b<-Cpp_count_gte(sorted, scores, threads)
  
  
  
  bind_cols(
    scores_df,
    p = (b+1)/(length(sorted)+1)
  )
}


permutation_p <- function(indexes, statistics, 
  min_pick = 2, max_pick = 10,
  n_permut=9999, 
  func = NULL, param = NULL, decreasing = NULL,
  seed=NULL, jump=NULL, exact_n_permut=FALSE,
  threads = 1) {

  indexes_by_npick<-get_indexes_by_n(indexes)
  indexes_by_npick<-filter_get_indexes_by_n(
    indexes_by_npick, min_n = min_pick, max_n = max_pick)
  mat_by_npick<-as_matrix_indexes_by_n(indexes_by_npick)
  map_dfr(mat_by_npick, 
    ~permutation_p_frommat(.x, statistics,
      n_permut=n_permut, func=func, param=param, decreasing=decreasing,
      seed=seed, jump=jump,
      exact_n_permut=exact_n_permut, threads=threads))
}

# this function calls permutation_p for mageck-style alpha-rra 
# parameters:
#   indexes: list of indexes statistics to genes; can be named
#   statistics: statistics used to rank gRNAs; will be converted to normalised ranks
#   rank_postoneg: statistics ranked in positive-to-negative (default=TRUE) or 
#     negative-to-positive order (rank_postoneg = FALSE), i.e.
#     if rank_postoneg = TRUE, most positive item has lowest rank (closest to zero)
#     if rank_postoneg = FALSE, most negative item has lowest rank (closest to zero)
#     lower ranked items (closest to 0) are more significant
#   alpha: percent alpha level for RRA; normalised ranks above this not scored
#          (except if this is first normalised rank); 
#          if NULL, statistics_p and p_threshold used to determine alpha
#   statistics_p: p-value for statistics, used to derive alpha in conjuction with
#          p_threshold; alpha must be NULL for this to be used
#          NB: norm_rank of last item with statistics_p<threshold is used as alpha
#          no check of ordering of statistics/p-values performed
#   p_threshold: p-value threshold for statistics; 
#   statistics_p_use: logical vector whether or not to use p-value in alpha calculation
#          use case: if p-values are two-tailed, set to FALSE where pvalues for
#          are for statistics in the direction that is not of current interest
#          (alternative p-values that are not of interest can be set to NA in statistics_p)
#   alpha_by_proportion: if TRUE, alpha is determined by proportion of 
#          statistics_p that are < threshold applied to ordered norm_ranks;
#          otherwise alpha determined by ordering according to statistics_p
#          and picking first norm_rank below threshold, which requires norm_ranks
#          and statistics_p to have the same rank order
#   min_pick/max_pick: permutation p value only obtained for items in `indexes`
#     that have >= min and <= max number of subitems
#     (e.g. by default permutations only done for genes with >=2 gRNAs and <= gRNAs)
#   n_permut: (minimum) number of permutations; 
#   seed: seed for permutations (to vary, jump can be changed instead of seed)
#   jump: used to set RNG stream; streams jump*2 and jump*2 + 1 used
#   exact_n_permut: perform exactly n_permut permutations; otherwise n_permut is
#     minimum and a small number of additional permutations made be performed
#     due to algorithm design
#   threads: number of threads to use in several calls
permutation_p_alpharra <- function(
  indexes, statistics, rank_postoneg = TRUE,
  alpha = NULL, 
  statistics_p = NULL, p_threshold = NULL,
  statistics_p_use = NULL,
  alpha_by_proportion = FALSE,
  min_pick = 2, max_pick = 10,
  n_permut=9999, 
  seed=permut_default_seed, jump=0, 
  exact_n_permut=FALSE,
  threads = 1) {
  
  stats_df<-tibble(
    stat = statistics,
  ) %>% 
  mutate(
    # converts values into normalised ranks as per Kolde 2012
    ranks = rank(stat*ifelse(rank_postoneg,-1,+1)),
    norm_ranks = ranks/max(ranks)
  )

  if (is.null(alpha)) {
    if (is.null(statistics_p) | is.null(p_threshold)) {
      stop("either alpha or statistics_p+p_threshold must be non-NULL")
    } else {
      # determine statistic with largest p that is < threshold
      stats_df<-bind_cols(
        stats_df,
        as_tibble_col(statistics_p, column_name = "p")
      )
      if (!is.null(statistics_p_use)) {
        # incorporate statistics_p_use, if prsent
        stats_df<-bind_cols(
          stats_df,
          as_tibble_col(statistics_p_use, column_name = "p_use")
        ) %>%
          mutate(
            orig_p = p,
            p = ifelse(p_use, p, NA) # set any that aren't of interest to NA
          )
      }
      if (alpha_by_proportion) {
        message("alpha by proportion only")
        count_below_threshold <- sum(stats_df$p<p_threshold, na.rm=TRUE)
        
        stats_df_arranged<-arrange(stats_df,p,norm_ranks)%>% # sort by norm_ranks only
          filter(!is.na(p)) # remove p values that are not of interest
        last_statistic<-stats_df_arranged[count_below_threshold,]
      } else {
        
        stats_df_arranged<-arrange(stats_df,p,norm_ranks)%>% # sort by p then norm_ranks for ties
          filter(!is.na(p)) %>% # remove p values that are not of interest
          mutate(rank_increasing = (c(norm_ranks[-1], Inf) - norm_ranks)>=0)
        # sanity-check, norm-rank and p in same order
        if (!all(stats_df_arranged$rank_increasing)) {
          stop(str_c(
            str_glue("When ordering by p, normalised rank are not in ascending order, "),
            str_glue("alpha cannot be estimated. "),
            str_glue("Check statistics and statistics_p are in the correct order.")
            ))
          #return(stats_df_arranged)
            
        }
        last_statistic<-stats_df%>% filter(p<p_threshold) %>% slice_max(p, with_ties = FALSE)
      }
      if (nrow(last_statistic)==0) {
        message("Warning: no statistics_p below p_threshold, alpha is 0.0")
        alpha<-0.0
      } else {
        alpha<-last_statistic$norm_ranks
        message(str_c(
          str_glue("setting alpha to {format(alpha,digits=4)} at "),
          str_glue("p={format(last_statistic$p,digits=4)} (<{p_threshold}) and "),
          str_glue("statistic={format(last_statistic$stat,digits=4)}")
        ))
      }

        
    }
  } else {
    if (!is.null(statistics_p) | !is.null(p_threshold)) {
      message("Warning: statistics_p and p_threshold not used if alpha is not NULL")
    }
  }
  permutation_p(
    indexes, stats_df$norm_ranks,
    func = "alpharra", param = alpha, decreasing = NULL,
    min_pick = min_pick, max_pick = max_pick,
    n_permut = n_permut,
    seed=seed, jump=jump, exact_n_permut=exact_n_permut,
    threads = threads
  )
}  

# this function calls permutation_p taking mean of statistics for items in indexes
# signif_descending for this function is defined in terms of significance, i.e.
#   signif_descending=TRUE means more significant items (lower p) have a more negative score
#   signif_descending=FALSE means more significant items (lower p) have a more positive score
# NB: this is the OPPOSITE definition to rank_postoneg for alpha_rra version
permutation_p_mean <- function(
  indexes, statistics, signif_descending = TRUE,
  min_pick = 2, max_pick = 10,
  n_permut=9999, 
  seed=permut_default_seed, jump=0, 
  exact_n_permut=FALSE,
  threads = 1) {
  
  permutation_p(
    indexes, statistics,
    func = "mean", param = alpha, decreasing = signif_descending,
    min_pick = min_pick, max_pick = max_pick,
    n_permut = n_permut,
    seed=seed, jump=jump, exact_n_permut=exact_n_permut,
    threads = threads
  )
}  

# this function calls permutation_p taking median of statistics for items in indexes
permutation_p_median <- function(
  indexes, statistics, signif_descending = TRUE,
  min_pick = 2, max_pick = 10,
  n_permut=9999, 
  seed=permut_default_seed, jump=0, 
  exact_n_permut=FALSE,
  threads = 1) {
  
  permutation_p(
    indexes, statistics,
    func = "median", param = alpha, decreasing = signif_descending,
    min_pick = min_pick, max_pick = max_pick,
    n_permut = n_permut,
    seed=seed, jump=jump, exact_n_permut=exact_n_permut,
    threads = threads
  )
}  


# this function calls permutation_p taking weighted mean of statistics for items in indexes
# statistics must be a matrix with values in first row and weights in second row
permutation_p_weightedmean <- function(
  indexes, statistics, signif_descending = TRUE,
  min_pick = 2, max_pick = 10,
  n_permut=9999, 
  seed=permut_default_seed, jump=0, 
  exact_n_permut=FALSE,
  threads = 1) {
  
  if (!is.matrix(statistics) | isTRUE(nrow(statistics)!=2)) {
    stop("statistics must be a matrix with values in row 1 and weights in row 2")
    
  }
  
  permutation_p(
    indexes, statistics,
    func = "weighted.mean", param = alpha, decreasing = signif_descending,
    min_pick = min_pick, max_pick = max_pick,
    n_permut = n_permut,
    seed=seed, jump=jump, exact_n_permut=exact_n_permut,
    threads = threads
  )
}  
