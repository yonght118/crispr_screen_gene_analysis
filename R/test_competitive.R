library(magrittr)
library(tidyverse)
library(furrr)

plan(multiprocess)
#plan(sequential) # uncomment to change plan

import::here(sample_and_permut,
             .from="R/sample_func.R")

import::here(norm_ranks, alpha_rra_mageck,
            .from="R/alpha_rra.R")

import::here(alpha_ks_signed,
             .from="R/ks_test.R")

import::here(alpha_ad_1way, alpha_ad,
             .from="R/ad_test.R")


# load test data
if (FALSE) {
  test_grna<-readr::read_tsv("test_data/eb_blocked_all_453_TDM1_cutoff0.1138_pos.txt")
    # cutoff for pos is 0.11383
    # cutoff for neg is 0.08547
  test_gene<-readr::read_tsv("test_data/eb_blocked_all_453_TDM1_pos_gene.txt")
}


# add normalised ranks
test_grna %<>% mutate(
    norm_rank_pos = norm_ranks(t),
    norm_rank_neg = norm_ranks(-t)
  )


# add n per group
test_grna %<>% group_by(grna_target) %>% add_count() %>% ungroup

# select only those with 4 per group
test_grna_group4 <- test_grna %>% filter(n==4) %>% droplevels()

alpha_rra_output_pos<-sample_and_permut(test_grna_group4, norm_rank_pos, function(x){-alpha_rra_mageck(x, 0.11383)[1]}, n_permut=1e7)
  # use NEGATIVE alpha rra score as alpha_rra scores are smaller for more significant hits

alpha_rra_output_neg<-sample_and_permut(test_grna_group4, norm_rank_neg, function(x){-alpha_rra_mageck(x, 0.08547)[1]}, n_permut=1e7)
  
alpha_ks_output_pos<-sample_and_permut(test_grna_group4, norm_rank_pos, function(x){alpha_ks_signed(x, 0.11383)[1]}, n_permut=1e7)
alpha_ad_output_pos<-sample_and_permut(test_grna_group4, norm_rank_pos, function(x){alpha_ad(x, 0.11383)[1]}, n_permut=1e7)

alpha_rra_output_pos$results%>%arrange(p)
alpha_ks_output_pos$results%>%arrange(p)
alpha_ad_output_pos$results%>%arrange(p)
  
