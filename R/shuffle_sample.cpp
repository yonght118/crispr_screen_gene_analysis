#include <RcppArmadillo.h>
#include <omp.h>
// [[Rcpp::depends(RcppArmadillo)]]

Rcpp::List Cpp_shuffle_sample_getn(
  const unsigned int n_elems, 
  const unsigned int n_pick, 
  const unsigned int n_permut,
  const double tuning_factor) {
shuffle_sample_getn<-function(elems, pick, N, tuning=1000, 
  shuffle_factor=12, min_shuffles = shuffle_factor) {
  # clamp lowerbound of tuning factor to value that uses same amount of memory
  # as no shuffle version
  tuning_at_1mem<-(elems/pick)^2/N
  if (tuning<tuning_at_1mem) {
    tuning<-tuning_at_1mem
  }

  # initial estimates
  n_sample_pre<-min(N, max(1, ceiling(sqrt(N*tuning)) ) )
  n_shuffle_pre<-ceiling(N/n_sample_pre)

  # round n_shuffle to next lowest factor of shuffle_factor, at least min_shuffles
  min_shuffles<-min(N, max(1,min_shuffles))
  n_shuffle<-max(min_shuffles, floor(n_shuffle_pre/shuffle_factor)*shuffle_factor)
  n_sample<-ceiling(N/n_shuffle)

  list(
    n_shuffle=n_shuffle, 
    n_sample=n_sample
    )
}
