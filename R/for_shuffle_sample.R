Rcpp::sourceCpp("R/for_shuffle_sample.cpp")
# install.packages("RcppXPtrUtils")
f_accu<-RcppXPtrUtils::cppXPtr("double foo(arma::vec a) { return accu(a); }", depends="RcppArmadillo")

if (FALSE) {
  n_permut<-1e7
  seed<-6278945
  n_elements_per_group<-4
  
  n_elements<-length(elements)
  n_divisions <- floor(min(ceiling(n_permut**(1/3)), 1000)) # do cube root or up to 1000 divisions
  n_permut_per_div <- ceiling(n_permut/n_divisions)
  sample_times<-n_permut_per_div
  shuffle_times<-n_divisions
  
  set.seed(seed)

  samples<-map(1:sample_times, ~sample.int(n_elements, n_elements_per_group, replace = FALSE, useHash = TRUE))
  shuffles<-map(1:shuffle_times, ~sample(elements, n_elements))
  
  samples_mat<-matrix(unlist(samples), nrow=length(samples[[1]]))
  shuffles_mat<-matrix(unlist(shuffles), nrow=length(shuffles[[1]]))
}


for_shuffle_sample_R <- function(shuffles_mat, samples_mat) {
  n_shuffles<-ncol(shuffles_mat)
  n_samples<-ncol(samples_mat)
  result <- numeric(n_shuffles*n_samples)
  for (i in 1:n_shuffles) {
    .shuffle<-shuffles_mat[,i]
    result_i<-(i-1)*n_samples
    for (j in 1:n_samples) {
      .sample<-samples_mat[,j]
      result[result_i+j]<-sum(.shuffle[.sample])
    }
  }
  result
  
}
r5<-for_shuffle_sample_R(shuffles_mat, samples_mat)
#testthat::expect_equal(r2,r5)

r6<-Cpp_shuffle_sample_for_accu_cpp(shuffles_mat, samples_mat-1)
r7<-Cpp_shuffle_sample_for_Rfunc_cpp(shuffles_mat, samples_mat-1, sum)
#testthat::expect_equal(r5,r6)
#testthat::expect_equal(r5,r7)

r8<-Cpp_shuffle_sample_for_cppfunc_cpp(shuffles_mat, samples_mat-1, f_accu)
testthat::expect_equal(r5,r8)

microbenchmark::microbenchmark(
  for_shuffle_sample_R(shuffles_mat, samples_mat),
  Cpp_shuffle_sample_for_accu_cpp(shuffles_mat, samples_mat-1),
  Cpp_shuffle_sample_for_Rfunc_cpp(shuffles_mat, samples_mat-1, sum),
  Cpp_shuffle_sample_for_cppfunc_cpp(shuffles_mat, samples_mat-1, f_accu),
  times=5, check="equal"
)
# Unit: milliseconds
#                                                                            expr       min        lq      mean    median        uq       max neval cld
#                                 for_shuffle_sample_R(shuffles_mat, samples_mat) 7272.8427 7399.9346 7414.4187 7440.5072 7449.4064 7509.4024     5 a  
#                  Cpp_shuffle_sample_for_accu_cpp(shuffles_mat, samples_mat - 1)  153.6730  155.7944  161.6772  158.7325  168.5839  171.6024     5  b 
#       Cpp_shuffle_sample_for_Rfunc_cpp(shuffles_mat, samples_mat -      1, sum) 6651.8351 6697.0304 6768.8835 6719.3111 6839.7215 6936.5196     5   c
#  Cpp_shuffle_sample_for_cppfunc_cpp(shuffles_mat, samples_mat -      1, f_accu)  234.1411  238.6046  240.8003  238.6735  242.0964  250.4858     5  b 


microbenchmark::microbenchmark(
  for_shuffle_sample_R(shuffles_mat, samples_mat),
  Cpp_shuffle_sample_for_accu_cpp(shuffles_mat, samples_mat-1,4),
  Cpp_shuffle_sample_for_Rfunc_cpp(shuffles_mat, samples_mat-1, sum),
  Cpp_shuffle_sample_for_cppfunc_cpp(shuffles_mat, samples_mat-1, f_accu,4),
  times=5, check="equal"
)

# Unit: milliseconds
#                                                                               expr       min        lq       mean    median        uq       max neval cld
#                                    for_shuffle_sample_R(shuffles_mat, samples_mat) 7906.9197 7974.8309 7991.25394 8007.6223 8033.2709 8033.6259     5 a  
#             Cpp_shuffle_sample_for_accu_cpp(shuffles_mat, samples_mat - 1,      4)   59.3999   59.5634   59.62912   59.5703   59.7718   59.8402     5  b 
#          Cpp_shuffle_sample_for_Rfunc_cpp(shuffles_mat, samples_mat -      1, sum) 7364.0336 7411.4215 7419.41816 7414.2789 7433.6710 7473.6858     5   c
#  Cpp_shuffle_sample_for_cppfunc_cpp(shuffles_mat, samples_mat -      1, f_accu, 4)   82.5664   82.9266   87.37664   83.1684   84.8889  103.3329     5  b 
