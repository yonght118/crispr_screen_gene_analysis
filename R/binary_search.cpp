#include <RcppArmadillo.h>
#include <omp.h>
// [[Rcpp::depends(RcppArmadillo)]]

template<typename T>
inline int count_gte_T(
    const arma::Col<T>& sorted,
    const T value) {
  arma::uword lo = 0;
  arma::uword total = sorted.n_elem;
  arma::uword hi = total;
  while (lo!=hi) {
    arma::uword mid = ((lo+hi)/2);
    if (sorted(mid)>=value) {
      hi = mid;
    } else {
      lo = mid+1;
    }
  }
  return(total-lo);
}

// [[Rcpp::export]]
Rcpp::IntegerVector Cpp_count_gte(
    const arma::dvec& sorted,
    const arma::dvec& values,
    const int n_threads = 1
    ) {
  arma::uword n = values.n_elem;
  arma::uvec result = arma::uvec(n);
  #pragma omp parallel for num_threads(n_threads)
  for (arma::uword i=0; i<n; i++) {
    result(i) = count_gte_T<double>(sorted, values(i));
  }
  
  return Rcpp::IntegerVector(result.begin(), result.end());;
}

template<typename T>
inline int count_lte_T(
    const arma::Col<T>& sorted,
    const T value) {
  arma::uword lo = 0;
  arma::uword total = sorted.n_elem;
  arma::uword hi = total;
  while (lo!=hi) {
    arma::uword mid = ((lo+hi)/2);
    if (sorted(mid)<=value) {
      hi = mid;
    } else {
      lo = mid+1;
    }
  }
  return(total-lo);
}

// [[Rcpp::export]]
Rcpp::IntegerVector Cpp_count_lte(
    const arma::dvec& sorted,
    const arma::dvec& values,
    const int n_threads = 1
    ) {
  arma::uword n = values.n_elem;
  arma::uvec result = arma::uvec(n);
  #pragma omp parallel for num_threads(n_threads)
  for (arma::uword i=0; i<n; i++) {
    result(i) = count_lte_T<double>(sorted, values(i));
  }
  
  return Rcpp::IntegerVector(result.begin(), result.end());;
}