library(tidyverse)
library(furrr)

# return number of in .sortedvector that are >= value
# .sortedvector must be sorted in ascending order
# uses binary search to run in O (log n)
# this version does a single lookup
count_gte_1 <- function(.sortedvector, .value) {
  lo <- 0
  hi <- length(.sortedvector)
  while (lo!=hi) {
    i <- ceiling((lo+hi)/2)
    if (.sortedvector[i]>=.value) {
      hi <- i -1
    } else {
      lo <- i
    }
  }
  return(length(.sortedvector)-lo)
}
# use_furrr has high overhead, only really beneficial for large number of lookups >1e5
count_gte <- function(sorted, values, use_furrr=FALSE) {
  n_values <- length(values)
  if (use_furrr) {
    n_workers<-nbrOfWorkers()
    if (n_workers>n_values) n_workers<-n_values
  } else {
    n_workers<-1
  }

  if (n_workers==1) {
    result <- integer(n_values)
    for (i in 1:n_values) {
      result[i]<-count_gte_1(sorted, values[i])
    }
    result
  } else {
    work_list<-divide_work(n_values, n_workers)
    worker_func<-function(start, end, ...) {
      n_values <- end-start+1
      result <- integer(n_values)
      for (i in 1:n_values) {
        result[i]<-count_gte_1(sorted, values[i+start-1])
      }
      result
    }
    future_pmap(work_list, worker_func) %>% unlist
  }
}
