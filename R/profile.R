# elements<-runif(100)
# n_elements_per_group<-4
# sample.int(n_elements, n_elements_per_group, replace=TRUE)
competitive_null_distribution<-function(elements, n_elements_per_group, test_stat_func = mean, n_permut = 1e7, seed = NULL) {
  # shuffle-sample strategy
  # generate n_divisions shuffled versions of elements
  # generate n_permut_per_div * sampled indexes of length n_elements_per_group
  # permutations can then be obtained using shuffle[sample]
  n_elements<-length(elements)
  n_divisions <- floor(min(ceiling(n_permut**(1/3)), 1000)) # do cube root or up to 1000 divisions
  n_permut_per_div <- ceiling(n_permut/n_divisions)
  sample_times<-n_permut_per_div
  shuffle_times<-n_divisions
  
  set.seed(seed)

  samples<-map(1:sample_times, ~sample.int(n_elements, n_elements_per_group, replace = FALSE, useHash = TRUE))
  shuffles<-map(1:shuffle_times, ~sample(elements, n_elements))
  
  stats_permut<-
    future_map_dbl(shuffles, function(.shuffle){
      map_dbl(samples, function(.sample){
        #sum(.sample)
        sum(.shuffle[.sample])
      })  
    })%>%unlist
  
  # may have > requested # of permutations, truncate prior to sorting
  length(stats_permut)<-n_permut
  #sort(stats_permut)
  data.table::fsort(stats_permut)
}


furrr_permut<-function(shuffles, samples) {
  future_map(shuffles, function(.shuffle){
    map_dbl(samples, function(.sample){
      #sum(.sample)
      sum(.shuffle[.sample])
    })  
  })%>%unlist
}

for_permut<-function(shuffles, samples) {
  n_shuffles<-length(shuffles)
  n_samples<-length(samples)
  result <- numeric(n_shuffles*n_samples)
  for (i in 1:n_shuffles) {
    .shuffle<-shuffles[[i]]
    result_i<-(i-1)*n_samples
    for (j in 1:n_samples) {
      .sample<-samples[[j]]
      result[result_i+j]<-sum(.shuffle[.sample])
    }
  }
  result
} 

library(doFuture)
# this variant uses nested foreach with dofuture
dopar_permut<-function(shuffles, samples) {
  foreach(shuffle = shuffles, .combine = c) %:%  
    foreach(sample = samples, .combine = c) %dofuture%  {
      sum(shuffle[sample])
    }
}

dopar2_permut<-function(shuffles, samples) {
  n_samples<-length(samples)
  
  foreach(shuffle = shuffles, .combine = c) %dofuture%  {
    result <- numeric(n_samples)
    for (j in 1:n_samples) {
      .sample<-samples[[j]]
      result[j]<-sum(shuffle[.sample])
    }
    result
  }
  
}

r1<-
for_permut(
  map(1:100, ~1:1e6),
  map(1:1000, ~(1:10)*..1)
  )
r2<-
furrr_permut(
  map(1:100, ~1:1e6),
  map(1:1000, ~(1:10)*..1)
  )
r3<-
  dopar_permut(
  map(1:100, ~1:1e6),
  map(1:1000, ~(1:10)*..1)
)
r4<-
  dopar2_permut(
  map(1:100, ~1:1e6),
  map(1:1000, ~(1:10)*..1)
)
# dopar_permut is extremely slow
testthat::expect_equal(r1,r2)
testthat::expect_equal(r2,r3)
testthat::expect_equal(r2,r4)


profvis::profvis(competitive_null_distribution(runif(100), 5, sum, n_permut = 1e7, seed = 6278945))

r1<-for_permut(shuffles,samples)
r2<-furrr_permut(shuffles,samples)
r4<-dopar2_permut(shuffles,samples)
testthat::expect_equal(r1,r2)
testthat::expect_equal(r2,r4)

profvis::profvis(for_permut(shuffles,samples))
profvis::profvis(furrr_permut(shuffles,samples))

microbenchmark::microbenchmark(
  for_permut(shuffles,samples),
  furrr_permut(shuffles,samples),
  dopar2_permut(shuffles,samples),
  times=3
)

# Unit: seconds
#                              expr      min        lq      mean    median        uq       max neval cld
#     for_permut(shuffles, samples)  3.96961  4.005267  4.092861  4.040924  4.154486  4.268047     3 a  
#   furrr_permut(shuffles, samples) 10.91239 11.000598 11.030538 11.088804 11.089610 11.090416     3  b 
#  dopar2_permut(shuffles, samples) 15.23276 15.251419 15.281982 15.270074 15.306591 15.343108     3   c
