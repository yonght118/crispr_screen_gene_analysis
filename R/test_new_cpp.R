library(tidyverse)
Rcpp::sourceCpp("R/for_shuffle_sample.cpp")
source("R/shuffle_sample_getn.R")

import::here(norm_ranks, alpha_rra_mageck,
            .from="R/alpha_rra.R")

tmp1<-shuffle_sample_getn(1e5, 4, N=1e7)
tmp2<-shuffle_sample_do(1e5, 4, n_shuffle=tmp1$n_shuffle, n_sample=tmp1$n_sample,
  seed=77919715)
tmp3<-runif(1e5)
tmp2a<-matrix(tmp3[tmp2$shuffle_mat], nrow=nrow(tmp2$shuffle_mat))
tmp2b<-matrix(as.numeric(tmp2$sample_mat-1), nrow=nrow(tmp2$sample_mat))
f_accu<-RcppXPtrUtils::cppXPtr("double foo(arma::vec a) { return accu(a); }", depends="RcppArmadillo")



inline_alpharra_str<-"double inline_alpha_rra_base_scoreonly(arma::vec& norm_ranks) {
  double alpha = 0.1;
  bool min1rank = false;
  // min1rank: use at least 1 rank for scoring; same as MAGeCK implementation
  arma::vec sorted_ranks = arma::sort(norm_ranks);
  arma::uword n = sorted_ranks.n_elem;
  arma::uword n_good = 0;
  
  double rra_score = 1.0; // max pbeta can return is 1.0
  
  
  for (arma::uword i=0;i<n; i++) {
    bool below_alpha = sorted_ranks(i)<=alpha;
    if (below_alpha) n_good+=1;
    if (below_alpha || (i==0 && min1rank)) {
      double this_pbeta = R::pbeta (sorted_ranks(i), i+1, n-i, true, false);  
      if (this_pbeta<rra_score) rra_score = this_pbeta;
    }
  }
  
  return rra_score;
}"

f_alpharra<-RcppXPtrUtils::cppXPtr(
  inline_alpharra_str,
  depends="RcppArmadillo")

inline_alpharramageck_str<-"double inline_alpha_rra_base_scoreonly(arma::vec& norm_ranks) {
  double alpha = 0.1;
  bool min1rank = true;
  // min1rank: use at least 1 rank for scoring; same as MAGeCK implementation
  arma::vec sorted_ranks = arma::sort(norm_ranks);
  arma::uword n = sorted_ranks.n_elem;
  arma::uword n_good = 0;
  
  double rra_score = 1.0; // max pbeta can return is 1.0
  
  
  for (arma::uword i=0;i<n; i++) {
    bool below_alpha = sorted_ranks(i)<=alpha;
    if (below_alpha) n_good+=1;
    if (below_alpha || (i==0 && min1rank)) {
      double this_pbeta = R::pbeta (sorted_ranks(i), i+1, n-i, true, false);  
      if (this_pbeta<rra_score) rra_score = this_pbeta;
    }
  }
  
  return rra_score;
}"

f_alpharra_mageck<-RcppXPtrUtils::cppXPtr(
  inline_alpharramageck_str,
  depends="RcppArmadillo")


#furrr_for_shuffle_sample_R(tmp2$shuffle_mat, tmp2$sample_mat, func=c, res_mode="list", use_furrr=FALSE),
if (FALSE) {
  #tmp4<-Cpp_shuffle_sample_for_accu_cpp(tmp2a, tmp2b,1)
  #tmp5<-Cpp_shuffle_sample_for_accu2_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1,1)
  #head(tmp4)
  #head(tmp5)
  microbenchmark::microbenchmark(
    Cpp_shuffle_sample_for_accu_cpp(tmp2a, tmp2b,1),
    Cpp_shuffle_sample_for_accu2_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1,1),
    Cpp_shuffle_sample_for_accu3_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1,1),
    times=3, check="equal"
    
  )
  
# Unit: milliseconds
#                                                                                       expr      min       lq     mean
#                                           Cpp_shuffle_sample_for_accu_cpp(tmp2a, tmp2b, 1) 181.5725 183.8220 184.8622
#  Cpp_shuffle_sample_for_accu2_cpp(tmp3, tmp2$shuffle_mat - 1,      tmp2$sample_mat - 1, 1) 373.6234 396.5322 406.6437
#  Cpp_shuffle_sample_for_accu3_cpp(tmp3, tmp2$shuffle_mat - 1,      tmp2$sample_mat - 1, 1) 214.0394 215.1189 229.1226
#    median       uq      max neval cld
#  186.0715 186.5070 186.9425     3  a 
#  419.4410 423.1539 426.8668     3   b
#  216.1983 236.6642 257.1300     3  a 
  #accu = directly supplied shuffles (of raw data) and samples (in double; converted in int)
  #accu2 = supplied shuffled indexes and sampled indexes; data looked up by data(shuffle(sample)) in function
  #accu3 = supplied shuffled indexes and sampled indexes; shuffles looked up by data(shuffle indexes) then indexed in inner loop
  #accu3 is obvious optimisation of accu2
  
  microbenchmark::microbenchmark(
    Cpp_shuffle_sample_for_accu3_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1,1),
    Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, f_accu, 1),
    times=3, check="equal"
    
  )
  # Unit: milliseconds
  # expr      min
  # Cpp_shuffle_sample_for_accu3_cpp(tmp3, tmp2$shuffle_mat - 1,      tmp2$sample_mat - 1, 1) 231.0787
  # Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat -      1, tmp2$sample_mat - 1, f_accu, 1) 360.6297
  # lq     mean   median       uq      max neval cld
  # 235.6211 248.6760 240.1635 257.4747 274.7859     3  a 
  # 361.5652 409.7585 362.5007 434.3229 506.1451     3   b  
  # accu3 is as above
  # Cpp_shuffle_sample_for_cppfuncdbl_cpp call uses supplied function that calls accu, so has additional
  # functional call but allows flexibility of function
}

microbenchmark::microbenchmark(
  furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat),
  Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, f_accu, 1),
  times=1, check="equal"
)

microbenchmark::microbenchmark(
  furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat),
  Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, f_accu, 1),
  times=1, check="equal"
)

# Unit: milliseconds
#                                                                                                    expr       min
#                                                      furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat) 8202.9956
#  Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat -      1, tmp2$sample_mat - 1, f_accu, 1)  358.3728
#         lq      mean    median        uq       max neval
#  8202.9956 8202.9956 8202.9956 8202.9956 8202.9956     1
#   358.3728  358.3728  358.3728  358.3728  358.3728     1

plan(multisession, workers=12)
alpha_rra01<-function(x) {alpha_rra(x, 0.1)[1]}
microbenchmark::microbenchmark(
  furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat, func = alpha_rra01),
  Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, f_alpharra, 1),
  Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, f_alpharra, 12),
  times=3, check="equal"
)


# Unit: milliseconds
#                                                                                                         expr        min
#                                       furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat, func = alpha_rra01) 44388.1953
#   Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat -      1, tmp2$sample_mat - 1, f_alpharra, 1)  1484.8124
#  Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat -      1, tmp2$sample_mat - 1, f_alpharra, 12)   201.6113
#         lq      mean     median         uq        max neval cld
#  44402.570 44499.855 44416.9440 44555.6848 44694.4256     3 a  
#   1486.405  1507.701  1487.9975  1519.1458  1550.2940     3  b 
#    205.395   209.075   209.1787   212.8069   216.4351     3   c


plan(multisession, workers=12)
alpha_rra01mageck<-function(x) {alpha_rra_mageck(x, 0.1)[1]}
microbenchmark::microbenchmark(
  furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat, func = alpha_rra01mageck),
  Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, f_alpharra_mageck, 1),
  Cpp_shuffle_sample_for_alpharra_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 0.1, 1),
  times=3, check="equal"
)
# above is non-mageck version (at least one normranks used)

# Unit: seconds
#                                                                                                               expr       min        lq      mean   median
#                                       furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat, func = alpha_rra01mageck) 49.182081 49.207739 49.287008 49.23340
#  Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat -      1, tmp2$sample_mat - 1, f_alpharra_mageck, 1)  3.654209  3.655875  3.657373  3.65754
#                  Cpp_shuffle_sample_for_alpharra_cpp(tmp3, tmp2$shuffle_mat -      1, tmp2$sample_mat - 1, 0.1, 1)  3.645752  3.653206  3.659946  3.66066
#         uq       max neval cld
#  49.339472 49.445547     3  a 
#   3.658955  3.660369     3   b
#   3.667043  3.673426     3   b

microbenchmark::microbenchmark(
  Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, f_alpharra, 12),
  Cpp_shuffle_sample_for_cppfuncdbl_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, f_alpharra_mageck, 12),
  times=1
)

## NEW

microbenchmark::microbenchmark(
  Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 1),
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 1),
  times=10, check="equal"
)

# Unit: milliseconds
#                                                                                      expr      min       lq     mean   median       uq      max neval cld
#  Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 1) 228.9366 237.4252 238.8918 238.6445 240.8532 244.9402    10  a 
#   Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 1) 361.4455 362.3103 364.3460 364.1285 365.3310 369.3028    10   b
# accu has direct implementation of accu in function body
# sum is templated version with accu provided in template

microbenchmark::microbenchmark(
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 1),
  Cpp_shuffle_sample_for_mean_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 1),
  Cpp_shuffle_sample_for_median_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 1),
  times=10
)
# 
# Unit: milliseconds
#                                                                                        expr       min        lq      mean    median        uq       max neval cld
#     Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 1)  345.7731  347.6116  360.0927  359.4787  373.3558  375.3714    10 a  
#    Cpp_shuffle_sample_for_mean_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 1)  387.5298  400.5856  405.5549  408.7469  411.9061  418.4035    10  b 
#  Cpp_shuffle_sample_for_median_cpp(tmp3, tmp2$shuffle_mat - 1,      tmp2$sample_mat - 1, 1) 1083.7630 1088.2625 1096.7454 1096.6676 1103.4406 1110.3218    10   c
# different functions


microbenchmark::microbenchmark(
  Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12),
  Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12),
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12),
  Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 6),
  Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 6),
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 6),
  Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 1),
  Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 1),
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 1),
  times=10, check="equal"
)
# Unit: milliseconds
#                                                                                        expr      min       lq      mean    median       uq      max neval   cld
#   Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 12)  77.5318  81.0181  91.50218  82.94985  88.1431 133.6290    10 a    
#  Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2$shuffle_mat - 1,      tmp2$sample_mat - 1, 12) 109.3641 112.3091 118.19035 113.63265 115.9992 159.3661    10 ab   
#    Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 12)  90.4591  91.0517 112.82371  95.59825 142.9307 149.6709    10 ab   
#    Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 6)  81.8824  84.6278  99.34897  93.14030 105.5399 145.1814    10 a    
#   Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2$shuffle_mat - 1,      tmp2$sample_mat - 1, 6) 132.6806 134.3942 140.39837 135.31395 138.7293 183.2707    10  bc  
#     Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 6) 105.6285 156.0091 152.29694 162.13175 164.2087 172.2319    10   c  
#    Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 1) 223.8889 226.0184 238.28382 228.48560 233.8428 279.3713    10    d 
#   Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2$shuffle_mat - 1,      tmp2$sample_mat - 1, 1) 227.6696 228.6891 241.94382 231.60145 234.8613 288.7103    10    d 
#     Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 1) 350.7824 352.0926 357.98841 352.62920 354.0229 404.7848    10     e
# accu is version with parallel outer loop (shuffle loop)
# accui is version with parallel inner loop (sample loop)
# sum is templated version with accu provided in template (parallel in shuffle loop)
# clearly parallel outer loop is better use of parallelisation


tmp2_shuffle_1<-(tmp2$shuffle_mat)[,1, drop=FALSE]
tmp2_sample_big<-matrix(rep(tmp2$sample_mat,ncol(tmp2$shuffle_mat)), nrow=nrow(tmp2$sample_mat))
ncol(tmp2_shuffle_1)*ncol(tmp2_sample_big)
object.size(tmp2_shuffle_1)+object.size(tmp2_sample_big) # 160400944 bytes

tmp2_shuffle_96<-matrix(rep((tmp2$shuffle_mat)[,1, drop=FALSE], 96), nrow=nrow(tmp2_shuffle_1))
tmp2_sample_big96<-tmp2_sample_big[,1:(ncol(tmp2_sample_big)/96)]
ncol(tmp2_shuffle_96)*ncol(tmp2_sample_big96)
object.size(tmp2_shuffle_96)+object.size(tmp2_sample_big96) # 40067104 bytes

microbenchmark::microbenchmark(
  Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2_shuffle_1-1, tmp2_sample_big-1, 12),
  Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2_shuffle_1-1, tmp2_sample_big-1, 12),
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2_shuffle_1-1, tmp2_sample_big-1, 12),
  Cpp_shuffle_sample_for_accuionly_cpp(tmp3, tmp2_sample_big-1, 12),
  
  Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2_shuffle_96-1, tmp2_sample_big96-1, 12),
  Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2_shuffle_96-1, tmp2_sample_big96-1, 12),
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2_shuffle_96-1, tmp2_sample_big96-1, 12),

  Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2_shuffle_1-1, tmp2_sample_big-1, 6),
  Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2_shuffle_1-1, tmp2_sample_big-1, 6),
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2_shuffle_1-1, tmp2_sample_big-1, 6),
  Cpp_shuffle_sample_for_accuionly_cpp(tmp3, tmp2_sample_big-1, 6),
  
  Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2_shuffle_96-1, tmp2_sample_big96-1, 6),
  Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2_shuffle_96-1, tmp2_sample_big96-1, 6),
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2_shuffle_96-1, tmp2_sample_big96-1, 6),
  
  Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2_shuffle_1-1, tmp2_sample_big-1, 1),
  Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2_shuffle_1-1, tmp2_sample_big-1, 1),
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2_shuffle_1-1, tmp2_sample_big-1, 1),
  Cpp_shuffle_sample_for_accuionly_cpp(tmp3, tmp2_sample_big-1, 1),
  
  Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2_shuffle_96-1, tmp2_sample_big96-1, 1),
  Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2_shuffle_96-1, tmp2_sample_big96-1, 1),
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2_shuffle_96-1, tmp2_sample_big96-1, 1),
  
  times=10, check="equal"
)
# first versions use samples only; second versions use both; same number of samples in total+
# identical data
# accuionly versions do not have an outer loop (as opposed to a single outer loop) and direct data lookup (data(samples))
# as opposed (to lookup data(shuffles(samples)))
# cld calculation took more than 5 seconds
#   set include_cld = FALSE to skip the cld calculation
# Unit: milliseconds
#                                                                                         expr      min       lq      mean    median       uq      max neval     cld
#      Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2_shuffle_1 - 1, tmp2_sample_big -      1, 12) 299.7193 302.0739 346.73172 305.83995 412.2218 434.5136    10 a      
#     Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2_shuffle_1 - 1, tmp2_sample_big -      1, 12) 155.6152 157.7454 169.23121 161.94940 169.3978 229.1423    10  bcde  
#       Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2_shuffle_1 - 1, tmp2_sample_big -      1, 12) 423.9842 427.2816 472.99689 433.20850 529.7923 643.7738    10      f 
#                     Cpp_shuffle_sample_for_accuionly_cpp(tmp3, tmp2_sample_big -      1, 12) 149.9283 155.6799 184.59814 159.17825 214.8469 270.6258    10   cde  
#   Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2_shuffle_96 - 1, tmp2_sample_big96 -      1, 12)  75.8654  78.7990  92.76558  82.93460  84.2814 189.8653    10       g
#  Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2_shuffle_96 - 1, tmp2_sample_big96 -      1, 12) 108.9814 110.4771 130.03961 112.63415 127.1052 238.8720    10   c   g
#    Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2_shuffle_96 - 1, tmp2_sample_big96 -      1, 12)  87.4317  90.3792 103.78306  92.81315  95.7401 206.2344    10  b    g
#       Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2_shuffle_1 - 1, tmp2_sample_big -      1, 6) 298.4540 302.5608 335.07014 304.70945 386.8609 443.2710    10 a      
#      Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2_shuffle_1 - 1, tmp2_sample_big -      1, 6) 167.0568 169.1195 208.56380 184.29505 276.2264 292.1803    10    de  
#        Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2_shuffle_1 - 1, tmp2_sample_big -      1, 6) 420.6050 426.6946 474.30499 454.54420 529.1688 568.7296    10      f 
#                      Cpp_shuffle_sample_for_accuionly_cpp(tmp3, tmp2_sample_big -      1, 6) 160.8669 165.3479 184.35681 168.26535 179.0859 282.1939    10   cde  
#    Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2_shuffle_96 - 1, tmp2_sample_big96 -      1, 6)  81.0825  83.6934  91.78117  87.77795  88.9191 132.3850    10       g
#   Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2_shuffle_96 - 1, tmp2_sample_big96 -      1, 6) 129.0012 136.1662 157.80423 139.71105 146.3652 242.7076    10    d  g
#     Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2_shuffle_96 - 1, tmp2_sample_big96 -      1, 6) 105.8317 106.9438 112.99158 111.41405 114.1968 131.4719    10   c   g
#       Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2_shuffle_1 - 1, tmp2_sample_big -      1, 1) 300.0203 305.7990 335.81643 306.19605 393.2725 423.0049    10 a      
#      Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2_shuffle_1 - 1, tmp2_sample_big -      1, 1) 302.1737 305.7438 346.12234 320.13555 382.9332 425.6924    10 a      
#        Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2_shuffle_1 - 1, tmp2_sample_big -      1, 1) 421.7819 424.6840 476.12498 464.34460 526.8973 551.2179    10      f 
#                      Cpp_shuffle_sample_for_accuionly_cpp(tmp3, tmp2_sample_big -      1, 1) 297.7750 300.7260 355.15526 348.39085 414.8075 419.9882    10 a      
#    Cpp_shuffle_sample_for_accu_cpp(tmp3, tmp2_shuffle_96 - 1, tmp2_sample_big96 -      1, 1) 219.8566 221.7456 225.82415 225.01990 229.0674 232.8878    10    de  
#   Cpp_shuffle_sample_for_accui_cpp(tmp3, tmp2_shuffle_96 - 1, tmp2_sample_big96 -      1, 1) 221.0041 225.2207 240.03551 227.95195 232.8379 350.8831    10     e  
#     Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2_shuffle_96 - 1, tmp2_sample_big96 -      1, 1) 347.2633 349.3102 393.96910 363.74680 442.7931 475.6735    10 a      
# As expected, without shuffles, accu version doesn't speed up with threads
# But speed up with shuffles and outer loop parallel is faster: 225, 134, 110 with 1, 6, 12 threads
# vs samples only: 300,170,160
# also for some reason even with one thread shuffles+samples version is slightly faster 225 vs 300
# accuionly version = little change vs accui
# Verdict: Shuffles algo is more efficient than pure samples algo; both performance wise and memory wise

microbenchmark::microbenchmark(
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12),
  Cpp_shuffle_sample_for_rsum_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12),
  times=20, check="equal"
)

# Unit: milliseconds
#                                                                                       expr     min       lq     mean
#   Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 12) 89.0679 103.6288 112.3936
#  Cpp_shuffle_sample_for_rsum_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 12) 99.9717 105.4269 115.0043
#    median       uq      max neval cld
#  109.5071 110.3010 224.1445    20   a
#  114.2274 123.3898 133.8344    20   a
# Original version (top, versus Row version, bottom)
# Row version is to allow extension to Mat (index via .Cols instead of () ), very minor slowdown

microbenchmark::microbenchmark(
  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12),
  furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat, func = sum),
  times=1, check="equal"
)

microbenchmark::microbenchmark(
  Cpp_shuffle_sample_for_mean_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12),
  furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat, func = mean),
  times=1, check="equal"
)

microbenchmark::microbenchmark(
  Cpp_shuffle_sample_for_median_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12),
  furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat, func = median),
  times=1, check="equal"
)

# Unit: milliseconds
#                                                                                      expr       min        lq      mean
#  Cpp_shuffle_sample_for_sum_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 12)  110.5549  110.5549  110.5549
#                            furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat, func = sum) 3654.3599 3654.3599 3654.3599
#     median        uq       max neval
#   110.5549  110.5549  110.5549     1
#  3654.3599 3654.3599 3654.3599     1

# Unit: milliseconds
#                                                                                       expr       min        lq      mean
#  Cpp_shuffle_sample_for_mean_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 12)  111.1056  111.1056  111.1056
#                            furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat, func = mean) 7746.5819 7746.5819 7746.5819
#     median        uq       max neval
#   111.1056  111.1056  111.1056     1
#  7746.5819 7746.5819 7746.5819     1
# Unit: seconds

#                                                                                         expr       min        lq
#  Cpp_shuffle_sample_for_median_cpp(tmp3, tmp2$shuffle_mat - 1,      tmp2$sample_mat - 1, 12)  1.120772  1.120772
#                            furrr_for_shuffle_sample_R(tmp2a, tmp2$sample_mat, func = median) 30.329655 30.329655
#       mean    median        uq       max neval
#   1.120772  1.120772  1.120772  1.120772     1
#  30.329655 30.329655 30.329655 30.329655     1

# all with new versions (Row) of functions
# results consistent with R

tmp3w1<-rbind(
  tmp3,
  rep(5, length(tmp3))
)

microbenchmark::microbenchmark(
  Cpp_shuffle_sample_for_mean_cpp(tmp3, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12),
  Cpp_shuffle_sample_for_wmean_cpp(tmp3w1, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12),
  times=20, check="equal"
)

# Unit: milliseconds
#                                                                                          expr      min       lq     mean
#     Cpp_shuffle_sample_for_mean_cpp(tmp3, tmp2$shuffle_mat - 1, tmp2$sample_mat -      1, 12) 107.3884 116.3153 121.3464
#  Cpp_shuffle_sample_for_wmean_cpp(tmp3w1, tmp2$shuffle_mat - 1,      tmp2$sample_mat - 1, 12) 127.0347 129.9275 133.8659
#    median       uq      max neval cld
#  123.4125 125.1243 132.6470    20  a 
#  131.5972 137.9189 149.6252    20   b

# weighted mean, uniform weights

tmp3w05<-rbind(
  tmp3,
  ifelse(tmp3>0.5,1,0.001)
)


microbenchmark::microbenchmark(
  Cpp_shuffle_sample_for_sum_cpp(tmp3w05[1,]*tmp3w05[2,], tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12)/
    Cpp_shuffle_sample_for_sum_cpp(tmp3w05[2,], tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12),
  Cpp_shuffle_sample_for_wmean_cpp(tmp3w05, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12),
  times=20, check="equal"
)
# Unit: milliseconds
#                                                                                                                                                                                                              expr
#  Cpp_shuffle_sample_for_sum_cpp(tmp3w05[1, ] * tmp3w05[2, ], tmp2$shuffle_mat -      1, tmp2$sample_mat - 1, 12)/Cpp_shuffle_sample_for_sum_cpp(tmp3w05[2,      ], tmp2$shuffle_mat - 1, tmp2$sample_mat - 1, 12)
#                                                                                                                     Cpp_shuffle_sample_for_wmean_cpp(tmp3w05, tmp2$shuffle_mat -      1, tmp2$sample_mat - 1, 12)
#       min       lq     mean   median       uq      max neval cld
#  235.5307 241.7176 262.9505 247.6948 251.4667 371.7162    20  a 
#  118.6945 123.0533 130.3574 131.9629 136.0970 141.3442    20   b

# weighted mean test for arbitrary weights

testthat::expect_equal(
  Cpp_shuffle_sample_for_wmean_cpp(tmp3w05, tmp2$shuffle_mat-1, tmp2$sample_mat-1, 12)[23913],
  tmp3w05[,tmp2$sample_mat[,23913]] %>% (function(.mat) {
    weighted.mean(.mat[1,], .mat[2,]) 
  })()
  
)
# check random result is correct