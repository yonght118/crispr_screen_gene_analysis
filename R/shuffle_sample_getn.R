permut_default_seed<-
  as.vector(c(-1291061711,-790653692), mode="integer")



# this function determines the number of shuffles and samples to do
# based on elems (number of elements), pick (number of elements to pick)
# and N (number of permutations)

# if shuffles and samples are equally expensive, then doing sqrt(N) of each
# would minimise memory and work done

# tuning is the tuning factor, with number of samples increased by sqrt(tuning)
# this accounts for shuffles being more expensive than samples; 
# tuning can be estimated as the relative cost of shuffles vs samples
# tuning of Inf forces minimum number of shuffles (below)
# tuning is clamped at lower end to prevent memory usage greater than that
# with only samples

# shuffle_factor: the number of shuffles is rounded down to a factor of 
# shuffle_factor; this is motivated by division of work in multiple parallel
# workers to process shuffles, shuffle_factor can be set to a value that 
# equally divides work between workers

# min_shuffles: at least this many shuttles must be performed (must be >=1)
shuffle_sample_getn<-function(elems, pick, N, tuning=1000, 
  shuffle_factor=12, min_shuffles = shuffle_factor) {
  # clamp lowerbound of tuning factor to value that uses same amount of memory
  # as no shuffle version
  tuning_at_1mem<-(elems/pick)^2/N
  if (tuning<tuning_at_1mem) {
    tuning<-tuning_at_1mem
  }

  # initial estimates
  n_sample_pre<-min(N, max(1, ceiling(sqrt(N*tuning)) ) )
  n_shuffle_pre<-ceiling(N/n_sample_pre)

  # round n_shuffle to next lowest factor of shuffle_factor, at least min_shuffles
  min_shuffles<-min(N, max(1,min_shuffles))
  n_shuffle<-max(min_shuffles, floor(n_shuffle_pre/shuffle_factor)*shuffle_factor)
  n_sample<-ceiling(N/n_shuffle)

  list(
    n_shuffle=n_shuffle, 
    n_sample=n_sample
    )
}

# calculate shuffles of elems
# first item is always identity
# uses seed
shuffle_do <- function(elems, n_shuffle, 
  rngkind=NULL, seed=NULL, jump=0) {
  
  if (!is.null(rngkind)) {
    dqrng::dqRNGkind(rngkind)
  }

  if (!is.null(seed)) {
    dqrng::dqset.seed(seed, jump*2)
  }
  
  shuffle_mat<-matrix(as.integer(0), nrow=elems, ncol=n_shuffle)
  shuffle_mat[,1]<-1:elems
  
  if (n_shuffle>1) {
    for (j in 2:n_shuffle) {
      shuffle_mat[,j]<-dqrng::dqsample.int(elems, elems)
    }
  }

  shuffle_mat
}

sample_do <- function(elems, pick, n_sample,
  rngkind=NULL, seed=NULL, jump=0) {
  
  if (!is.null(rngkind)) {
    dqrng::dqRNGkind(rngkind)
  }

  if (!is.null(seed)) {
    dqrng::dqset.seed(seed, jump*2)
  }
  
  sample_mat<-matrix(as.integer(0), nrow=pick, ncol=n_sample)
  for (j in 1:n_sample) {
    sample_mat[,j]<-dqrng::dqsample.int(elems, pick)
  }
  sample_mat
}

# default seed generated from 64 random bits
shuffle_sample_do <- function(elems, pick, n_shuffle, n_sample, 
  seed=permut_default_seed, jump=0, rngkind="xoroshiro128+") {
  list(
    shuffle_mat=shuffle_do(elems, n_shuffle, rngkind = rngkind, seed = seed, jump = jump),
    sample_mat=sample_do(elems, pick, n_sample, rngkind = rngkind, seed = seed, jump = jump)
  )
}
