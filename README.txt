Gene analysis methods to combine gRNA statistics can be obtained by running
source("_GeneLevelAnalysis_topackage.R", chdir=TRUE)
which will also call upon permutation functions in R/load.R


# session info

R version 4.3.2 (2023-10-31 ucrt)
Platform: x86_64-w64-mingw32/x64 (64-bit)
Running under: Windows 10 x64 (build 19045)

Matrix products: default


locale:
[1] LC_COLLATE=English_New Zealand.utf8  LC_CTYPE=English_New Zealand.utf8    LC_MONETARY=English_New Zealand.utf8
[4] LC_NUMERIC=C                         LC_TIME=English_New Zealand.utf8    

time zone: Pacific/Auckland
tzcode source: internal

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] limma_3.58.1    furrr_0.3.1     future_1.33.2   Hmisc_5.1-2     lubridate_1.9.3 forcats_1.0.0   stringr_1.5.1  
 [8] dplyr_1.1.4     purrr_1.0.2     readr_2.1.5     tidyr_1.3.1     tibble_3.2.1    ggplot2_3.5.0   tidyverse_2.0.0
[15] magrittr_2.0.3 

loaded via a namespace (and not attached):
 [1] tidyselect_1.2.1         Exact_3.2                rootSolve_1.8.2.4        farver_2.1.1            
 [5] fastmap_1.1.1            digest_0.6.35            rpart_4.1.21             timechange_0.3.0        
 [9] lifecycle_1.0.4          cluster_2.1.4            statmod_1.5.0            lmom_3.0                
[13] compiler_4.3.2           rlang_1.1.3              tools_4.3.2              utf8_1.2.4              
[17] data.table_1.15.4        knitr_1.46               labeling_0.4.3           dqrng_0.3.2             
[21] htmlwidgets_1.6.4        bit_4.0.5                RColorBrewer_1.1-3       expm_0.999-9            
[25] withr_3.0.0              foreign_0.8-85           nnet_7.3-19              grid_4.3.2              
[29] fansi_1.0.6              e1071_1.7-14             colorspace_2.1-0         globals_0.16.3          
[33] scales_1.3.0             MASS_7.3-60              cli_3.6.2                mvtnorm_1.2-4           
[37] rmarkdown_2.26           crayon_1.5.2             ragg_1.3.0               generics_0.1.3          
[41] rstudioapi_0.16.0        httr_1.4.7               tzdb_0.4.0               RcppArmadillo_0.12.8.2.0
[45] readxl_1.4.3             gld_2.6.6                proxy_0.4-27             parallel_4.3.2          
[49] cellranger_1.1.0         base64enc_0.1-3          vctrs_0.6.5              boot_1.3-28.1           
[53] Matrix_1.6-5             hms_1.1.3                bit64_4.0.5              Formula_1.2-5           
[57] htmlTable_2.4.2          listenv_0.9.1            systemfonts_1.0.6        glue_1.7.0              
[61] parallelly_1.37.1        codetools_0.2-19         stringi_1.8.3            gtable_0.3.4            
[65] munsell_0.5.1            pillar_1.9.0             htmltools_0.5.8.1        R6_2.5.1                
[69] textshaping_0.3.7        vroom_1.6.5              evaluate_0.23            lattice_0.21-9          
[73] backports_1.4.1          DescTools_0.99.54        class_7.3-22             Rcpp_1.0.12             
[77] gridExtra_2.3            checkmate_2.3.1          xfun_0.43                pkgconfig_2.0.3   